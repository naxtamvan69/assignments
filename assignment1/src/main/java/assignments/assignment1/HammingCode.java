package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * Encode program for Hamming Code.
     * membuat while loop untuk mencari banyaknya bit redundan
     * membuat for loop untuk meletakkan sementara nilai 0 di dua pangkat bit redundan.
     * hal ini untuk memudahkan ketika menjumlahkan dari tiap-tiap posisi dua pangkat bit redundan.
     * membuat nested loop
     * for loop pertama untuk menempatkan nilai 1 atau 0 dari posisi dua pangkat bit redundan.
     * loop kedua untuk menambahkan bit-bit yang dicek sesuai posisi dua pangkat bit redundan.
     */
    public static String encode(String data) {
        int bitRedundan = 0;
        boolean cek = true;
        while (cek) {
            int powBitRedundan = (int) Math.pow(2, bitRedundan);
            if (powBitRedundan >= data.length() + bitRedundan + 1) {
                cek = false;
            } else {
                bitRedundan++;
            }
        }
        for (int i = 0; i < bitRedundan; i++) {
            int parity = (int)Math.pow(2, i);
            data = data.substring(0, parity - 1) + "0" + data.substring(parity - 1);
        }
        for (int i = 1; i <= bitRedundan; i++) {
            int sumBits = 0;
            int parity = (int)Math.pow(2, i - 1);
            for (int posBit = 1; posBit <= data.length(); posBit++) {
                int cekPosition = posBit / parity;
                if (cekPosition % 2 == 1) {
                    sumBits += Integer.parseInt(data.substring(posBit - 1, posBit));
                }
            }
            if (sumBits % 2 == 0) {
                data = data.substring(0, parity - 1) + "0" + data.substring(parity);
            } else {
                data = data.substring(0, parity - 1) + "1" + data.substring(parity);
            }
        }
        return data;
    }

    /**
     * Decode program for Hamming Code.
     * membuat while loop untuk mencari banyaknya bit redundan.
     * membuat while loop lagi untuk mengecek dan fix error dari posisi dua pangkat bit redundan.
     * untuk mengecek errornya dengan menggunaka nested for loop yang berada di dalam while loop.
     * for loop pertama untuk mencari posisi error dari dua pangkat bit redundan.
     * for loop kedua untuk menjumlahkan bit-bit yang dicek sesuai posisi dua pangkat bit redundan.
     * setelah itu mebuat for loop untuk posisi menghapus dua pangkat bit redundan.
     */
    public static String decode(String code) {
        int bitRedundan = 0;
        boolean cek = true;
        while (cek) {
            int powBitRedundan = (int)Math.pow(2, bitRedundan);
            if (powBitRedundan >= code.length() + 1) {
                cek = false;
            } else {
                bitRedundan++;
            }
        }
        boolean cekError = true;
        while (cekError) {
            int posBitError = 0;
            for (int i = 1; i <= bitRedundan; i++) {
                int sumBits = 0;
                int parity = (int)Math.pow(2, i - 1);
                for (int posBit = 1; posBit <= code.length(); posBit++) {
                    int cekPos = posBit / parity;
                    if (cekPos % 2 == 1) {
                        sumBits += Integer.parseInt(code.substring(posBit - 1, posBit));
                    }
                }
                if (sumBits % 2 == 0) {
                    posBitError += 0;
                } else {
                    posBitError += parity;
                }
            }
            if (posBitError == 0) {
                cekError = false;
            } else if (code.substring(posBitError - 1, posBitError).equals("1")) {
                code = code.substring(0, posBitError - 1) + "0" + code.substring(posBitError);
            } else if (code.substring(posBitError - 1, posBitError).equals("0")) {
                code = code.substring(0, posBitError - 1) + "1" + code.substring(posBitError);
            }
        }
        for (int remBitRedundan = bitRedundan - 1; 0 <= remBitRedundan; remBitRedundan--) {
            int posBitRedundan = (int)Math.pow(2, remBitRedundan);
            code = code.substring(0, posBitRedundan - 1) + code.substring(posBitRedundan);
        }
        return code;
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
