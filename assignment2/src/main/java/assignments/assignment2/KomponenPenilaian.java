package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    /**
     * Membuat objek KomponenPenilaian sebagai konstraktor.
     * @param nama sebagai nama komponen penilaian.
     * @param banyakButirPenilaian jumlah banyaknya value dari array butirPenilaian.
     * @param bobot sebagai banyaknya bobot dari nama Komponen Penilaian.
     */
    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    /**
     * Membuat objek masukkanButirPenilaian untuk mengisi index array butirPenilaian sesuai input.
     * @param idx sebagai index yang dipilih.
     * @param butir sebagai value untuk mengisi array butirPenilaian sesuai idx.
     */
    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        this.butirPenilaian[idx] = butir;
    }

    /**
     * Membuat objek getNama untuk mengembalikan nama Komponen Penilaian.
     * @return nama Komponen Penilaian.
     */
    public String getNama() {
        return this.nama;
    }

    /**
     * Membuat objek getRerata untuk mengembalikan rata-rata butirPenilaian.
     * @return rata-rata butirPenilaian.
     */
    public double getRerata() {
        int butir = 0;
        double nilai = 0;
        // melakukan iterasi dari array butiPenilaian.
        // jika di setiap iterasi bukan null maka var nilai akan ditambah.
        // serta butir akan ditambah satu (sesuai diinput asdos)
        for (ButirPenilaian list: butirPenilaian) {
            if (list != null) {
                nilai += list.getNilai();
                butir++;
            }
        }
        //jika var butir tidak nol maka nilai dibagi butir.
        //jika iya maka 0.
        return (butir != 0) ? nilai / butir : 0;
    }

    /**
     * Membuat objek getNilai untuk mengembalikan nilai Akhir komponenPenilaian.
     * @return getRerata * bobot(dalam pesen).
     */
    public double getNilai() {
        return this.getRerata() * this.bobot / 100;
    }

    /**
     * Membuat objek getDetail untuk mengembalikan string
     * nama komponen pernilaian, rerata, nilai akhir
     * dari masing-masing butirPenilaian sesuai dengan soal.
     * @return String str.
     */
    public String getDetail() {
        String str = String.format("~~~ %s (%d%%) ~~~\n", this.nama, this.bobot);
        int x = 1;
        for (ButirPenilaian list : butirPenilaian) {
            if (butirPenilaian.length != 1 & list != null) {
                str += String.format("%s %d: %s\n", this.nama, x, list);
                str += String.format("Rerata: %.2f\n", this.getRerata());
                str += String.format("Kontribusi nilai akhir: %.2f\n\n", this.getNilai());
                x++;
            } else if (butirPenilaian.length == 1 & list != null) {
                str += String.format("%s: %s\n", this.nama, list);
                str += String.format("Kontribusi nilai akhir: %.2f\n\n", this.getNilai());
            } else {
                str += String.format("Nilai %s belum diinput\n\n", this.nama);
            }
        }
        return str;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah KomponenPenilaian sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return String.format("Rerata %s: %.2f", this.nama, this.getRerata());
    }

}