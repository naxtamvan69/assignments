package assignments.assignment2;


public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    /**
     * Membuat objek ButirPenilaian sebagai constructor.
     * @param nilai sebagai int nilai.
     * @param terlambat sebagai boolean terlambat.
     */
    public ButirPenilaian(double nilai, boolean terlambat) {
        // TODO: buat constructor untuk ButirPenilaian.
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    /**
     * Membuat objek getNilai untuk mengembalikan Nilai.
     * @return nilai.
     */
    public double getNilai() {
        // TODO: kembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        double nilai = 0;
        //jika nilai >= 0 dan terlambat makan nilai - penalti terlambat.
        //jika nilainya minus maka nilai menjadi 0
        if (this.nilai >= 0) {
            if (this.terlambat) {
                nilai = this.nilai - (this.nilai * PENALTI_KETERLAMBATAN / 100);
            } else {
                nilai = this.nilai;
            }
        } else {
            nilai = 0;
        }
        return nilai;
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        if (this.terlambat) {
            return String.format("%.2f (T)", this.getNilai());
        } else {
            return String.format("%.2f", this.getNilai());
        }
    }
}
