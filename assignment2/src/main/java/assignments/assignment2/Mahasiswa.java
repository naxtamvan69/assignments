package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    /**
     * Membuat objek Mahasiswa sebagai constructor.
     * @param npm sebagai String npm mahasiswa.
     * @param nama sebagai String nama mahasiswa.
     * @param komponenPenilaian sebagai array KompenenPenilaian.
     */
    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    /**
     * Membuat objek getKomponenPenilaian untuk mengembalikan namaKomponen.
     * @return elemen dari array komponenPenilaian
     */
    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // jika tidak ada namaKomponen di komponenPenilaian, kembalikan null.
        KomponenPenilaian result = null;
        for (KomponenPenilaian list : komponenPenilaian) {
            if (list.getNama().equals(namaKomponen)) {
                result = list;
            }
        }
        return result;
    }

    /**
     * Membuat objek getNpm untuk mengembalikan NPM mahasiswa.
     * @return NPM Mahasiswa.
     */
    public String getNpm() {
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilaiAkhir) {
        return nilaiAkhir >= 85 ? "A" :
            nilaiAkhir >= 80 ? "A-" :
            nilaiAkhir >= 75 ? "B+" :
            nilaiAkhir >= 70 ? "B" :
            nilaiAkhir >= 65 ? "B-" :
            nilaiAkhir >= 60 ? "C+" :
            nilaiAkhir >= 55 ? "C" :
            nilaiAkhir >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    /**
     * Membuat objek rekap untuk mengembalikan rekapan sesuai permintaan soal.
     * terdiri dari rerata masing-masing komponenPenilaian.
     * Nilai Akhir dari kalkulasi masing-masing komponenPenilaian.
     * Huruf hasil dari nilai akhir
     * lulus/tidak lulus dari hasil nilai akhir.
     * @return String str.
     */
    public String rekap() {
        String str = String.format("");
        double nilaiAkhir = 0;
        for (KomponenPenilaian list: komponenPenilaian) {
            str += String.format("Rerata %s: %.2f\n", list.getNama(), list.getRerata());
            nilaiAkhir += list.getNilai();
        }
        str += String.format("Nilai Akhir: %.2f\nHuruf: %s\n%s",
                nilaiAkhir, getHuruf(nilaiAkhir), getKelulusan(nilaiAkhir));
        return str;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah KomponenPenilaian sesuai permintaan soal.
     * @return String.
     */
    public String toString() {
        return this.npm + " - " + this.nama;
    }

    /**
     * Membuat objek getDetail untuk mengembalikan string
     * terdiri dari object getDetail dari class KomponenPenilaian.
     * Nilai akhir dari kalkulasi beberapa nilai akhir komponenPenilaian.
     * Huruf dari hasil Nilai Akhir.
     * Lulus/tidak lulus dari hasil Nilai Akhir.
     * @return String str.
     */
    public String getDetail() {
        String str = "";
        double nilaiAkhir = 0;
        for (KomponenPenilaian list: komponenPenilaian) {
            str += String.format("%s", list.getDetail());
            nilaiAkhir += list.getNilai();
        }
        str += String.format("Nilai Akhir: %.2f\nHuruf: %s\n%s",
                nilaiAkhir, getHuruf(nilaiAkhir), getKelulusan(nilaiAkhir));
        return str;
    }

    /**
     * Membuat objek compareTo untuk membandingkan antar mahasiswa.
     * @return int result.
     */
    @Override
    public int compareTo(Mahasiswa other) {
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        int result = this.npm.compareTo(other.npm);
        return result;
    }
}
