package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    /**
     * Membuat objek AsistenDosen sebagai constructor.
     * @param kode sebagai kode asdos.
     * @param nama sebagai nama asdos.
     */
    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    /**
     * Membuat objek getKode untuk mengembalikan kode mahasiswa.
     * @return kode mahasiswa.
     */
    public String getKode() {
        return this.kode;
    }

    /**
     * Membuat objek addMahasiswa untuk menambahkan mahasiswa ke arraylist mahasiswa.
     * dengan catatan urutannya harus sesuai dengan npm mahasiswa.
     * @param mahasiswa sebagai identitas mahasiswa.
     */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        // membuat nested loop untuk membandingkan 2 mahasiswa
        // memanfaatkan compareTo pada class mahasiswa untuk memudahkan pengurutan.
        for (int i = 0; i < this.mahasiswa.size(); i++) {
            Mahasiswa banding1;
            Mahasiswa banding2;
            for (int j = 0; j < this.mahasiswa.size(); j++) {
                banding1 = this.mahasiswa.get(i);
                banding2 = this.mahasiswa.get(j);
                int k = banding1.compareTo(banding2);
                // jika hasil dari compareTo adalah negatif maka akan diurutkan sesuai NPM.
                if (k < 0) {
                    this.mahasiswa.set(i, banding2);
                    this.mahasiswa.set(j, banding1);
                }
            }
        }
        //Collections.sort(this.mahasiswa);
    }

    /**
     * Membuat objek removeMahasiswa untuk menghapus mahasiswa di arraylist mahasiswa.
     */
    public void removeMahasiswa(String npm) {
        for (int i = 0; i < this.mahasiswa.size(); i++) {
            Mahasiswa cek = this.mahasiswa.get(i);
            //jika NPM yang diinput ada di arraylist mahasiswa maka akan dihapus.
            if (cek.getNpm().equals(npm)) {
                this.mahasiswa.remove(i);
            }
        }
    }

    /**
     * Membuat objek getMahasiswa untuk mengembalikan objek Mahasiswa.
     * @return elemen dari arraylist mahasiswa.
     */
    public Mahasiswa getMahasiswa(String npm) {
        // jika tidak ada, kembalikan null atau lempar sebuah Exception.
        Mahasiswa result = null;
        for (Mahasiswa list : mahasiswa) {
            if (list.getNpm().equals(npm)) {
                result = list;
            }
        }
        return result;
    }

    /**
     * Membuat objek rekap untuk mengembalikan string sesuai permintaan soal.
     * terdiri dari method rekap dari mahasiswa.
     * @return String str.
     */
    public String rekap() {
        String str = "";
        int i = 1;
        for (Mahasiswa list : mahasiswa) {
            if (i != mahasiswa.size()) {
                str += String.format("%s\n%s\n\n", list, list.rekap());
                i++;
            } else {
                str += String.format("%s\n%s", list, list.rekap());
            }
        }
        return str;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah KomponenPenilaian sesuai permintaan soal.
     * @return String.
     */
    public String toString() {
        return this.kode + " - " + this.nama;
    }
}
