package assignments.assignment3;

public class CleaningService extends Manusia {
    private int jumlahDibersihkan;

    /**
     * Membuat objek CleaningService sebagai constructor.
     * @param nama sebagai nama CelaningService.
     */
    public CleaningService(String nama) {
        super(nama);
    }

    /**
     * Membuat objek bersihkan untuk membersihkan benda dari status Covid positif.
     * @param benda sebagai carrier bertipe benda.
     */
    public void bersihkan(Benda benda) {
        // jika persentase menular lebih dari 0
        // benda akan diubah menjadi status negatif
        // benda akan di set persentase menularnya jadi 0
        // jumlah benda yang dibersihkan oleh cleaning service akan ditambah
        if (benda.persentaseMenular >= 0) {
            benda.ubahStatus("negatif");
            benda.setPersentaseMenular(0);
            jumlahDibersihkan++;
        }
    }

    /**
     * Membuat objek getjumlahDibersihkan untuk mengembalikan jumlah benda yang telah bersih.
     * @return jumlah benda yang telah bersih oleh cleaning service.
     */
    public int getJumlahDibersihkan() {
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return jumlahDibersihkan;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah AngkutanUmum sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString()  {
        return "CLEANING SERVICE " + this.getNama();
    }
}