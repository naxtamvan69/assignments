package assignments.assignment3;

import java.io.FileInputStream;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class SimulatorGUI extends Application {
    private Button bt1;
    private Button bt3;
    private ComboBox<String> cb1;
    private ComboBox<String> cb2;
    private ComboBox<String> cb3;
    private ComboBox<String> cb4;
    private TextField tf1;
    private TextField tf2;
    private TextField tf3;
    private InputOutput io;
    private Stage stage;
    private TextArea ta;

    private VBox getPane11() throws Exception {
        VBox vb = new VBox();
        vb.setAlignment(Pos.CENTER);
        vb.setSpacing(15);
        Label label1 = new Label("Selamat datang di program simulasi COVID-19!");
        label1.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        FileInputStream fs = new FileInputStream("picture/corona.jpeg");
        Image image = new Image(fs);
        ImageView iv = new ImageView(image);
        iv.setFitHeight(150);
        iv.setFitWidth(300);
        vb.getChildren().addAll(label1, iv);
        return vb;
    }

    private GridPane getPane12() {
        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setAlignment(Pos.BASELINE_CENTER);
        gp.setPadding(new Insets(25, 25, 25, 25));

        Label lb1 = new Label("Silahkan masukkan metode input yang anda inginkan");
        Label slb11 = new Label("INPUT");
        Label slb12 = new Label("Name File:");
        Label lb2 = new Label("Silahkan masukkan metode output yang anda inginkan");
        Label slb21 = new Label("OUTPUT");
        Label slb22 = new Label("Name File:");
        gp.add(lb1, 0, 0, 5, 1);
        gp.add(slb11, 0, 1);
        gp.add(new Label(":"), 1, 1);
        gp.add(slb12, 3, 1);
        gp.add(lb2, 0, 2, 5, 1);
        gp.add(slb21, 0, 3);
        gp.add(new Label(":"), 1, 3);
        gp.add(slb22, 3, 3);

        String[] list = {"text", "terminal", "gui"};
        cb1 = new ComboBox<>();
        cb2 = new ComboBox<>();
        for (String x: list) {
            cb1.getItems().add(x);
            cb2.getItems().add(x);
        }
        cb2.getItems().remove("gui");
        gp.add(cb1, 2, 1);
        gp.add(cb2, 2, 3);
        cb1.setOnAction(event -> {
            String input1 = cb1.getValue();
            tf1.setDisable(!input1.equals("text"));
        });
        cb2.setOnAction(event -> {
            String input2 = cb2.getValue();
            tf2.setDisable(!input2.equals("text"));
        });

        tf1 = new TextField();
        tf2 = new TextField();
        tf1.setDisable(true);
        tf2.setDisable(true);
        tf1.setPromptText("File berformat .txt");
        tf2.setPromptText("File berformat .txt");
        gp.add(tf1, 4, 1);
        gp.add(tf2, 4, 3);

        bt1 = new Button("Submit");
        gp.add(bt1, 4,4);
        GridPane.setHalignment(bt1, HPos.RIGHT);
        btSetAction1();
        return gp;
    }

    private GridPane getPane13() throws Exception {
        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setAlignment(Pos.BASELINE_RIGHT);
        gp.setPadding(new Insets(25, 25, 25, 25));

        FileInputStream fs = new FileInputStream("picture/dekdepe.png");
        Image image = new Image(fs);
        ImageView iv = new ImageView(image);
        iv.setFitHeight(50);
        iv.setFitWidth(50);

        Label lb1 = new Label("Fakultas Ilmu Kompter Universitas Indonesia");
        Label lb2 = new Label("© 2020");
        GridPane.setHalignment(lb2, HPos.RIGHT);

        gp.add(lb1, 0, 0);
        gp.add(lb2, 0, 1);
        gp.add(iv, 1, 0, 1, 2);
        return gp;
    }

    private BorderPane getPane14() throws Exception {
        BorderPane bp = new BorderPane();
        bp.setTop(getPane11());
        bp.setCenter(getPane12());
        bp.setBottom(getPane13());
        return bp;
    }

    private GridPane getPane21() {
        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setAlignment(Pos.CENTER);
        gp.setPadding(new Insets(25, 25, 25, 25));

        Label lb1 = new Label("Query");
        Label lb2 = new Label("Tipe");
        Label lb3 = new Label("Objek");
        GridPane.setHalignment(lb1, HPos.CENTER);
        GridPane.setHalignment(lb2, HPos.CENTER);
        GridPane.setHalignment(lb3, HPos.CENTER);
        gp.add(lb1, 0, 0);
        gp.add(lb2, 1, 0);
        gp.add(lb3, 2, 0);

        cb3 = new ComboBox<>();
        cb4 = new ComboBox<>();
        cb4.setDisable(true);
        String[] query = {"ADD", "INTERAKSI", "POSITIFKAN", "SEMBUHKAN", "BERSIHKAN", "RANTAI",
                          "TOTAL_KASUS_DARI_OBJEK", "AKTIF_KASUS_DARI_OBJEK",
                          "TOTAL_SEMBUH_MANUSIA", "TOTAL_SEMBUH_PETUGAS_MEDIS",
                          "TOTAL_BERSIH_CLEANING_SERVICE", "EXIT"};
        for (String ls: query) {
            cb3.getItems().add(ls);
        }
        String[] objek = {"ANGKUTAN_UMUM", "CLEANING_SERVICE", "JURNALIS", "OJOL",
                          "PEGANGAN_TANGGA", "PEKERJA_JASA", "PETUGAS_MEDIS", "PINTU",
                          "TOMBOL_LIFT"};
        for (String ls: objek) {
            cb4.getItems().add(ls);
        }
        cb3.setOnAction(event -> {
            if (cb3.getValue().equals("ADD")) {
                cb4.setDisable(false);
                tf3.setDisable(false);
            } else {
                cb4.setDisable(true);
                tf3.setDisable(cb3.getValue().equals("EXIT")
                        || cb3.getValue().equals("TOTAL_SEMBUH_MANUSIA"));
                tf3.clear();
            }
        });
        gp.add(cb3, 0, 1);
        gp.add(cb4, 1, 1);

        tf3 = new TextField();
        tf3.setDisable(true);
        tf3.setPrefWidth(150);
        gp.add(tf3, 2, 1);

        bt3 = new Button("Submit");
        gp.add(bt3, 3, 1);

        Label op = new Label("OUTPUT:");
        gp.addRow(2, op);

        ta = new TextArea();
        ta.setEditable(false);
        ta.setPrefSize(100, 200);
        gp.add(ta, 0, 3, 4, 1);
        btSetAction2();
        return gp;
    }

    private BorderPane getPane22() throws Exception {
        VBox vbox = new VBox(5);
        Label label = new Label("MASUKKAN PERINTAH ANDA");
        label.setFont(new Font("Arial", 20));
        vbox.setAlignment(Pos.BOTTOM_CENTER);
        vbox.getChildren().addAll(label, getPane21());
        BorderPane bp = new BorderPane();
        bp.setCenter(vbox);
        bp.setBottom(getPane13());
        return bp;
    }

    private void btSetAction1() {
        bt1.setOnAction(event -> {
            boolean result = alertWarning1();
            if (!result) {
                String inputType = cb1.getValue();
                String outputType = cb2.getValue();
                String inputFile = "NOTHING";
                String outputFile = "NOTHING";
                if (outputType.equals("text")) {
                    outputType = tf2.getText();
                }
                if (inputType.equals("gui")) {
                    io = new InputOutput(inputType, inputFile, outputType, outputFile);
                    try {
                        stage.setScene(getScene2());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (inputType.equals("text")) {
                        inputFile = tf1.getText();
                    }
                    io = new InputOutput(inputType, inputFile, outputType, outputFile);
                    try {
                        io.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Platform.exit();
                }

            }
        });
    }

    private void btSetAction2() {
        bt3.setOnAction(event -> {
            boolean result = alertWarning2();
            if (!result) {
                String query = cb3.getValue();
                String objek = cb4.getValue();
                String[] cmd;
                if (query.equals("ADD")) {
                    String x = objek + " " + tf3.getText();
                    cmd = x.split(" ");
                } else {
                    cmd = tf3.getText().split(" ");
                }
                try {
                    io.runGUI(query, cmd);
                    tf3.clear();
                    ta.appendText(io.getCommand() + "\n");
                } catch (Exception e) {
                    e.printStackTrace();
                    ta.appendText(e.toString() + "\n");
                    ta.appendText("PROGRAM BERHENTI!!!!\n");
                    ta.appendText("SILAHKAN CLOSE PROGRAM ANDA");
                    cb3.setDisable(true);
                    cb4.setDisable(true);
                    tf3.setDisable(true);
                    bt3.setDisable(true);
                }
                if (query.equals("EXIT")) {
                    ta.appendText("SILAHKAN CLOSE PROGRAM ANDA");
                    cb3.setDisable(true);
                    cb4.setDisable(true);
                    tf3.setDisable(true);
                    bt3.setDisable(true);
                }
            }
        });
    }

    private boolean alertWarning1() {
        String bc1 = cb1.getValue();
        String bc2 = cb2.getValue();
        boolean result = false;
        if (bc1 == null || bc2 == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error Warning");
            alert.setHeaderText(null);
            alert.setContentText("You must choise Input or Output");
            alert.showAndWait();
            result = true;
        } else if (bc1.equals("text") && tf1.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error Warning");
            alert.setHeaderText(null);
            alert.setContentText("Input your FileInput");
            alert.showAndWait();
            result = true;
        } else if (bc2.equals("text") && tf2.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error Warning");
            alert.setHeaderText(null);
            alert.setContentText("Input your FileOutput");
            alert.showAndWait();
            result = true;
        }
        return result;
    }

    private boolean alertWarning2() {
        boolean result = false;
        String bc = cb3.getValue();
        if (bc == null) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error Warning");
            alert.setHeaderText(null);
            alert.setContentText("Choise your query");
            alert.showAndWait();
            result = true;
        } else if (cb3.getValue().equals("ADD") && cb4.getValue().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error Warning");
            alert.setHeaderText(null);
            alert.setContentText("Input your tipe");
            alert.showAndWait();
            result = true;
        } else if (!(cb3.getValue().equals("TOTAL_SEMBUH_MANUSIA") || cb3.getValue().equals("EXIT"))
                && tf3.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error Warning");
            alert.setHeaderText(null);
            alert.setContentText("Input your command");
            alert.showAndWait();
            result = true;
        }
        return result;
    }

    private Scene getScene1() throws Exception {
        return new Scene(getPane14(), 800, 500);
    }

    private Scene getScene2() throws Exception {
        return new Scene(getPane22(), 800, 500);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        stage.setTitle("SimulatorGUI");
        stage.setScene(getScene1());
        stage.setResizable(false);
        stage.show();
    }

    // test
    public static void main(String[] args) {
        launch();
    }

}
