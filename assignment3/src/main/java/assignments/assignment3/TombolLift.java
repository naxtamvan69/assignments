package assignments.assignment3;

public class TombolLift extends Benda {
    /**
     * Membuat objek TombolLift sebagai constructor.
     * @param nama sebagai nama tombol lift.
     */
    public TombolLift(String nama) {
        super(nama);
    }

    /**
     * Membuat objek tambahPersentase untuk menambahkan persentase penularan.
     * penularan bertambah sebanyak 20.
     */
    @Override
    public void tambahPersentase() {
        this.persentaseMenular += 20;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah tombol lift sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "TOMBOL LIFT " + this.getNama();
    }
}