package assignments.assignment3;

public class Ojol extends Manusia {
    /**
     * Membuat objek Ojol sebagai constructor.
     * @param nama sebagai nama Ojol.
     */
    public Ojol(String nama) {
        super(nama);
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah Ojol sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "OJOL " + this.getNama();
    }
}