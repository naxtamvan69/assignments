package assignments.assignment3;


public abstract class Benda extends Carrier {
    protected int persentaseMenular;

    /**
     * Membuat objek Benda sebagai constructor.
     * tipe Benda merupakan jenis Benda.
     * @param nama sebagai nama Benda.
     */
    public Benda(String nama) {
        super(nama, "Benda");
    }

    /**
     * Membuat objek tambahPersentase untuk menambahkan persentase penularan.
     * tambahPersentase adalah method abstract.
     * akan di-override ke subclassnya
     */
    public abstract void tambahPersentase();

    /**
     * Membuat objek getPersentaseMenular untuk mengembalikan persentase menular benda.
     * @return persentaseMenular.
     */
    public int getPersentaseMenular() {
        return persentaseMenular;
    }

    /**
     * Membuat objek setPersentaseMenular untuk mengubah persentaseMenular.
     * @param persentase sebagai jumlah persentase yang akan diubah.
     */
    public void setPersentaseMenular(int persentase) {
        persentaseMenular = persentase;
    }
}