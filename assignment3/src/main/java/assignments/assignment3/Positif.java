package assignments.assignment3;

public class Positif implements Status {
    /**
     * Membuat objek getStatus untuk mengembalikan status covid.
     * @return status positif.
     */
    public String getStatus() {
        return "Positif";
    }

    /**
     * Membuat objek tularkan untuk menularkan covid dari pernular ke tertular.
     * @param penular sebagai carrier penular.
     * @param tertular sebagai carrier tertular.
     */
    public void tularkan(Carrier penular, Carrier tertular) {
        String tipePenular = penular.getTipe();
        String tipeTertular = tertular.getTipe();
        // jika penular manusia dan tertular benda, maka manusia menularkan benda
        if (tipePenular.equals("Manusia") && tipeTertular.equals("Benda")) {
            Benda benda = (Benda) tertular;
            benda.tambahPersentase();
            // jika persentase menular lebih dari 100, maka benda akan menjadi positif
            // rantai penular dari manusia penular akan ditambah ke rantai penular benda tertular
            if (benda.getPersentaseMenular() >= 100) {
                tertular.ubahStatus("positif");
                for (Carrier list: penular.getRantaiPenular()) {
                    tertular.getRantaiPenular().add(list);
                }
                tertular.getRantaiPenular().add(tertular);
            }
        // jika penular manusia dan tertular manusia, maka manusia menularkan manusia
        } else if (tipePenular.equals("Manusia") && tipeTertular.equals("Manusia")) {
            // manusia yang tertular menjadi positif Covid
            // rantai penular dari manusia penular akan ditambah ke rantai penular manusia tertular
            tertular.ubahStatus("positif");
            for (Carrier list: penular.getRantaiPenular()) {
                tertular.getRantaiPenular().add(list);
            }
            tertular.getRantaiPenular().add(tertular);
        // jika penular benda dan tertular manusia, maka benda menularkan manusia
        } else if (tipePenular.equals("Benda") && tipeTertular.equals("Manusia")) {
            // manusia yang tertular menjadi positif Covid
            // rantai penular dari benda penular akan ditambah ke rantai penular manusia tertular
            tertular.ubahStatus("positif");
            for (Carrier list: penular.getRantaiPenular()) {
                tertular.getRantaiPenular().add(list);
            }
            tertular.getRantaiPenular().add(tertular);
        }
    }
}