package assignments.assignment3;

public class PeganganTangga extends Benda {
    /**
     * Membuat objek PeganganTangga sebagai constructor.
     * @param nama sebagai nama Pegangangan tangga.
     */
    public PeganganTangga(String nama) {
        super(nama);
    }

    /**
     * Membuat objek tambahPersentase untuk menambahkan persentase penularan.
     * penularan bertambah sebanyak 20.
     */
    @Override
    public void tambahPersentase() {
        this.persentaseMenular += 20;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah PeganganTangga sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "PEGANGAN TANGGA " + this.getNama();
    }
}