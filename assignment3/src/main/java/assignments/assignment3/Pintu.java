package assignments.assignment3;

public class Pintu extends Benda {
    /**
     * Membuat objek Pintu sebagai constructor.
     * @param nama sebagai nama Pintu.
     */
    public Pintu(String nama) {
        super(nama);
    }

    /**
     * Membuat objek tambahPersentase untuk menambahkan persentase penularan.
     * penularan bertambah sebanyak 30.
     */
    @Override
    public void tambahPersentase() {
        this.persentaseMenular += 30;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah pegangan tangga sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "PINTU " + this.getNama();
    }
}