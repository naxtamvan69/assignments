package assignments.assignment3;

public class PetugasMedis extends Manusia {
    private int jumlahDisembuhkan;

    /**
     * Membuat objek PetugasMedis sebagai constructor.
     * @param nama sebagai nama PetugasMedis.
     */
    public PetugasMedis(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    /**
     * Membuat objek obati untuk mengobati pasien positif Covid.
     * Mengubah status pasien positif menjadi negatif.
     * Menambah jumlah pasien yang disembuhkan oleh PetugasMedis.
     * Menambah jumlah manusia sembuh dari Covid.
     * @param manusia sebagai carrier manusia.
     */
    public void obati(Manusia manusia) {
        manusia.ubahStatus("negatif");
        jumlahDisembuhkan++;
        this.tambahSembuh();
    }

    /**
     * Membuat objek getjumlahDisembuhkan untuk mengembalikan jumlah manusia yang telah sembuh.
     * @return jumlah manusia yang telah semubh oleh petugas medis.
     */
    public int getJumlahDisembuhkan() {
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return jumlahDisembuhkan;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah PekerjaJasa sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "PETUGAS MEDIS " + this.getNama();
    }
}