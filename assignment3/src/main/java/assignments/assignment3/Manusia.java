package assignments.assignment3;

public abstract class Manusia extends Carrier {
    private static int jumlahSembuh = 0;

    /**
     * Membuat objek Manusia sebagai constructor.
     * tipe merupakan jenis tipe Manusia
     * @param nama sebagai nama objek.
     */
    public Manusia(String nama) {
        super(nama, "Manusia");
    }

    /**
     * Membuat objek tambahSembuh untuk menambahkan jumlah manusia sembuh.
     */
    public void tambahSembuh() {
        jumlahSembuh++;
    }

    /**
     * Membuat objek getJumlahSembuh untuk mengembalikan jumlah manusia sembuh.
     * @return jumlah manusia sembuh.
     */
    public static int getJumlahSembuh() {
        return jumlahSembuh;
    }
    
}