package assignments.assignment3;

public class Negatif implements Status {
    /**
     * Membuat objek getStatus untuk mengembalikan status covid.
     * @return status negatif.
     */
    public String getStatus() {
        return "Negatif";
    }

    /**
     * Membuat objek tularkan untuk menularkan covid dari pernular ke tertular.
     * method ini tidak digunakan karena tidak mungkin penular berstatus negatif.
     * @param penular sebagai carrier penular.
     * @param tertular sebagai carrier tertular.
     */
    public void tularkan(Carrier penular, Carrier tertular) {
        return;
    }
}