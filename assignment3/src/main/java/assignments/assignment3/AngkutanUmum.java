package assignments.assignment3;

public class AngkutanUmum extends Benda {
    /**
     * Membuat objek AngkutanUmum sebagai constructor.
     * @param nama sebagai nama AngkutanUmum.
     */
    public AngkutanUmum(String nama) {
        super(nama);
    }

    /**
     * Membuat objek tambahPersentase untuk menambahkan persentase penularan.
     * penularan bertambah sebanyak 35.
     */
    @Override
    public void tambahPersentase() {
        this.persentaseMenular += 35;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah AngkutanUmum sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "ANGKUTAN UMUM " + this.getNama();
    }
}