package assignments.assignment3;

public class Jurnalis extends Manusia {
    /**
     * Membuat objek Jurnalis sebagai constructor.
     * @param nama sebagai nama Jurnalis.
     */
    public Jurnalis(String nama) {
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah Jurnalis sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "JURNALIS " + this.getNama();
    }
}