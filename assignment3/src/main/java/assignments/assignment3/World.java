package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World {
    public List<Carrier> listCarrier;

    /**
     * Membuat objek World sebagai constructor.
     */
    public World() {
        listCarrier = new ArrayList<>();
    }

    /**
     * Membuat objek createObject untuk membuat carrier objek.
     * @param tipe sebagai jenis tipe objek.
     * @param nama sebagai nama objek.
     * @return carrier objek
     */
    public Carrier createObject(String tipe, String nama) {
        Carrier object = null;
        String tipeFix = tipe.toUpperCase();
        // mengecek tipe yang sesuai dengan tipe yang sudah ada di soal
        // jika sesuai makan akan dibuat objek carrier
        switch (tipeFix) {
            case "ANGKUTAN_UMUM":
                object = new AngkutanUmum(nama);
                break;
            case "CLEANING_SERVICE":
                object = new CleaningService(nama);
                break;
            case "JURNALIS":
                object = new Jurnalis(nama);
                break;
            case "OJOL":
                object = new Ojol(nama);
                break;
            case "PEGANGAN_TANGGA":
                object = new PeganganTangga(nama);
                break;
            case "PEKERJA_JASA":
                object = new PekerjaJasa(nama);
                break;
            case "PETUGAS_MEDIS":
                object = new PetugasMedis(nama);
                break;
            case "PINTU":
                object = new Pintu(nama);
                break;
            case "TOMBOL_LIFT":
                object = new TombolLift(nama);
                break;
            default:
                break;
        }
        return object;
    }

    /**
     * Membuat objek getCarrier untuk mengembalikan objek Carrier dengan nama tertentu.
     * @param nama sebagai nama objek
     * @return carrier yang sesuai dengan nama tertentu.
     */
    public Carrier getCarrier(String nama) {
        Carrier object = null;
        for (Carrier list: listCarrier) {
            String fixNama = nama.toUpperCase();
            String fixListNama = list.getNama().toUpperCase();
            if (fixListNama.equals(fixNama)) {
                object = list;
            }
        }
        return object;
    }
}
