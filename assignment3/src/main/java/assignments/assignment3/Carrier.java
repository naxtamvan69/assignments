package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier {
    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    /**
     * Membuat objek Carrier sebagai constructor.
     * @param nama sebagai nama objek.
     * @param tipe sebagai jenis tipe objek.
     */
    public Carrier(String nama,String tipe) {
        this.nama = nama;
        this.tipe = tipe;
        rantaiPenular = new ArrayList<>();
    }

    /**
     * Membuat objek getNama untuk mengembalikan nama objek.
     * @return nama.
     */
    public String getNama() {
        // TODO : Kembalikan nilai dari atribut nama
        return nama;
    }

    /**
     * Membuat objek getTipe untuk mengembalikan jenis tipe objek.
     * @return tipe.
     */
    public String getTipe() {
        return tipe;
    }

    /**
     * Membuat objek getStatusCovid untuk mengembalikan status covid objek.
     * @return status covid objek.
     */
    public String getStatusCovid() {
        return statusCovid.getStatus();
    }

    /**
     * Membuat objek getAktifKasusDisebabkan untuk mengembalikan jumlah tertular.
     * yang masih aktif disebabkan oleh objek penular
     * @return jumlah tertular aktif oleh penular.
     */
    public int getAktifKasusDisebabkan() {
        return aktifKasusDisebabkan;
    }

    /**
     * Membuat objek getTotalKasusDisebabkan untuk mengembalikan jumlah tertular.
     * yang masih aktif dan sudah sembuh disebabkan oleh objek penular
     * @return jumlah tertular aktif dan sembuh oleh penular.
     */
    public int getTotalKasusDisebabkan() {
        return totalKasusDisebabkan;
    }

    /**
     * Membuat objek getRantaiPenular untuk mengembalikan rantai penularan yang dialami tertular.
     * @return rantai penularan yang dialami oleh tertular.
     */
    public List<Carrier> getRantaiPenular() {
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return rantaiPenular;
    }

    /**
     * Membuat objek ubahStatus untuk mengubah status covid objek.
     * @param status sebagai status covid.
     */
    public void ubahStatus(String status) {
        // jika status merupakan positif, class Positif akan di Upcasting ke statusCovid
        if (status.equals("positif")) {
            statusCovid = new Positif();
        // jika status merupakan negatif, class Negatif akan di Upcasting ke statusCovid
        } else if (status.equals("negatif")) {
            statusCovid = new Negatif();
        }
    }

    /**
     * Membuat objek interaksi untuk melakukan interaksi antara penular dan tertular.
     * @param lain sebagai objek yang tertular.
     */
    public void interaksi(Carrier lain) {
        statusCovid.tularkan(this, lain);
        // membuat objek fix berisi elemen carier yang diambil dari rantaiPenular tertular
        // berfungsi untuk memudahkan menambah totalKasusDisebabkan dan aktifKasusDisebabkan
        // karena elemen carrier tidak akan yang sama
        ArrayList<Carrier> fix = new ArrayList<>(lain.getRantaiPenular());
        for (int i = 0; i < fix.size() - 1; i++) {
            for (int j = i + 1; j < fix.size() - 1; j++) {
                if (fix.get(i).equals(fix.get(j))) {
                    fix.remove(j);
                }
            }
        }
        int size = 1;
        // melakukan iturasirasi dari fix untuk tambah totalKasusDisebabkan dan totalAktifDisebabkan
        for (Carrier list: fix) {
            // jika size itu sama jumlah elemen yang ada di fix maka break
            // digunakan untuk iterasi terakhir(tertular) karena tertular tidak akan ditambah
            if (size == fix.size()) {
                break;
            // ditambah untuk yang berinteraksi langsung
            }  else if (this.equals(list)) {
                totalKasusDisebabkan++;
                aktifKasusDisebabkan++;
                size++;
            // ditambah untuk yang berinteraksi tidak langsung
            } else {
                list.totalKasusDisebabkan++;
                list.aktifKasusDisebabkan++;
                size++;
            }
        }
    }

    /**
     * Membuat objek sembuh untuk mengurangi aktifKasusDisebabkan.
     * Karena terdapat objek yang sembuh atau bersih dari Covid
     * @param lain sebagai carrier yang sembuh atau sembuh.
     * @param blackList sebgai list yang berisi elemen yang telah dikurangi
     * @return carrier blacklist yang digunakan di objek lainnya
     */
    public ArrayList<Carrier> sembuh(Carrier lain, ArrayList<Carrier> blackList) {
        // membuat objek fix berisi elemen carier yang diambil dari rantaiPenular tertular
        // berfungsi untuk memudahkan menambah totalKasusDisebabkan dan aktifKasusDisebabkan
        // karena elemen carrier tidak akan yang sama
        ArrayList<Carrier> fix = new ArrayList<>(lain.getRantaiPenular());
        int z = 1;
        for (int i = 0; i < fix.size() - 1; i++) {
            for (int j = i + 1; j < fix.size() - 1; j++) {
                if (fix.get(i).equals(fix.get(j))) {
                    fix.remove(j);
                    z++;
                }
            }
        }
        int size = 1;
        // melakukan iturasirasi dari fix untuk kurangi totalAktifDisebabkan
        for (Carrier list: fix) {
            // jika carier berada di posisi pertama dan sama dengan carrier sembuh
            // maka di break
            if (fix.get(0).equals(this) && z == 1) {
                break;
            // jika size itu sama jumlah elemen yang ada di fix maka break
            // digunakan untuk iterasi terakhir(tertular) karena tertular tidak akan ditambah
            } else if (size == fix.size()) {
                break;
            // jika carier yang di fix ada di blacklist maka akan di skip
            // karena carier tersebut telah dikurangi aktifKasusKasusDisebabkan
            } else if (blackList.contains(list)) {
                continue;
            // jika elemen yang ada di fix itu sama dengan carrier sembuh maka di sklip
            } else if (this.equals(list)) {
                size++;
                continue;
            // aktifKasusDisebabkan dikurangi dan carrier tersebut akan ditambah ke black list
            } else {
                list.aktifKasusDisebabkan--;
                size++;
                blackList.add(list);
            }
        }
        return blackList;
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sesuai permintaan soal.
     * objek toString adalah abstrack maka akan di-override ke subclass.
     * @return String.
     */
    public abstract String toString();

}
