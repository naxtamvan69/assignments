package assignments.assignment3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class InputOutput {
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile; 
    private World world;
    private String command;

    /**
     * Membuat objek InputOutput sebagai constructor.
     * @param inputFile sebagai nama file input.
     * @param inputType sebagai jenis tipe input.
     * @param outputFile sebagai nama file output.
     * @param outputType sebagai jenis tipe output.
     */
    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        setBufferedReader(inputType);
        setPrintWriter(outputType);
    }

    /**
     * Membuat objek setBufferedReader untuk membaca file.
     * @param inputType sebagai jenis tipe input.
     */
    public void setBufferedReader(String inputType) {
        try {
            // jika jenis tipe input text, maka file akan dibaca
            if (inputType.toLowerCase().equals("text")) {
                br = new BufferedReader(new FileReader(inputFile));
            // jika jenis tipe input terminal, maka user akan menginput query di terminal.
            } else if (inputType.toLowerCase().equals("terminal")) {
                br = new BufferedReader(new InputStreamReader(System.in));
            } else if (inputType.toLowerCase().equals("gui")) {
                world = new World();
            }
            // karena file bisa saja tidak ada maka inputFile menjadi NOTFOUND
        } catch (IOException e) {
            inputFile = "NOTFOUND";
        }
    }

    /**
     * Membuat objek setPrintWriter untuk menulis file.
     * @param outputType sebagai jenis tipe output.
     */
    public void setPrintWriter(String outputType) {
        try {
            // jika jenis tipe output text, maka akan menulis file.
            if (outputType.toLowerCase().equals("text")) {
                pw = new PrintWriter(new FileWriter(outputFile));
            // jika jenis tipe output terminal, maka akan dicetak di terminal
            } else if (outputType.toLowerCase().equals("terminal")) {
                pw = new PrintWriter(System.out);
            }
            // karena file bisa saja tidak ada maka inputFile menjadi NOTFOUND
        } catch (IOException e) {
            outputFile = "NOTFOUND";
        }
    }

    /**
     * Membuat objek run untuk mmeminta input user.
     * dikhususkan untuk Simulator
     */
    public void run() throws IOException {
        world = new World();
        // jika inputFile atau output File tidak ditemukan maka kembalikan eror
        if (inputFile.equals("NOTFOUND") || outputFile.equals("NOTFOUND")) {
            throw new IOException("File not Found");
        } else {
            boolean hasChosenExit = false;
            // meminta input dari user
            String line = br.readLine();
            while (!hasChosenExit) {
                String[] str = line.split(" ");
                String query = str[0].toUpperCase();
                int size = str.length - 1;
                if (size == 2) {
                    switch (query) {
                        case "ADD":
                            add(str[1], str[2]);
                            line = br.readLine();
                            break;
                        case "INTERAKSI":
                            interaksi(str[1], str[2]);
                            line = br.readLine();
                            break;
                        case "SEMBUHKAN":
                            sembuhkan(str[1], str[2]);
                            line = br.readLine();
                            break;
                        case "BERSIHKAN":
                            bersihkan(str[1], str[2]);
                            line = br.readLine();
                            break;
                        default:
                            throw new IOException(query + " not Found");
                    }
                } else if (size == 1) {
                    switch (query) {
                        case "POSITIFKAN":
                            positifkan(str[1]);
                            line = br.readLine();
                            break;
                        case "RANTAI":
                            rantai(str[1]);
                            line = br.readLine();
                            break;
                        case "TOTAL_KASUS_DARI_OBJEK":
                            totalKasusDariObjek(str[1]);
                            line = br.readLine();
                            break;
                        case "AKTIF_KASUS_DARI_OBJEK":
                            aktifKasusdariObjek(str[1]);
                            line = br.readLine();
                            break;
                        case "TOTAL_SEMBUH_PETUGAS_MEDIS":
                            totalSembuhPetugasMedis(str[1]);
                            line = br.readLine();
                            break;
                        case "TOTAL_BERSIH_CLEANING_SERVICE":
                            totalBersihCleaningService(str[1]);
                            line = br.readLine();
                            break;
                        default: throw new IOException("Query not Found");
                    }
                } else if (size == 0) {
                    switch (query) {
                        case "TOTAL_SEMBUH_MANUSIA":
                            totalSembuhManusia();
                            line = br.readLine();
                            break;
                        case "EXIT":
                            hasChosenExit = exit();
                            break;
                        default: throw new IOException("Query not Found");
                    }
                }
            }
            br.close();
            pw.close();
        }
    }

    /**
     * Membuat objek runGUI untuk mmeminta input user.
     * dikhususkan untuk SimulatorGUI
     * @param query sebagai query
     * @param cmd sebagai perintah
     */
    public void runGUI(String query, String[] cmd) throws IOException {
        int size = cmd.length;
        // jika isi perintahnya kosong maka size akan jadi 0
        if (cmd[0].equals("")) {
            size = 0;
        }
        // meminta input dari user
        if (size == 2) {
            switch (query) {
                case "ADD":
                    add(cmd[0], cmd[1]);
                    break;
                case "INTERAKSI":
                    interaksi(cmd[0], cmd[1]);
                    break;
                case "SEMBUHKAN":
                    sembuhkan(cmd[0], cmd[1]);
                    break;
                case "BERSIHKAN":
                    bersihkan(cmd[0], cmd[1]);
                    break;
                default:
                    throw new IOException(query + " not Found");
            }
        } else if (size == 1) {
            switch (query) {
                case "POSITIFKAN":
                    positifkan(cmd[0]);
                    break;
                case "RANTAI":
                    rantai(cmd[0]);
                    break;
                case "TOTAL_KASUS_DARI_OBJEK":
                    totalKasusDariObjek(cmd[0]);
                    break;
                case "AKTIF_KASUS_DARI_OBJEK":
                    aktifKasusdariObjek(cmd[0]);
                    break;
                case "TOTAL_SEMBUH_PETUGAS_MEDIS":
                    totalSembuhPetugasMedis(cmd[0]);
                    break;
                case "TOTAL_BERSIH_CLEANING_SERVICE":
                    totalBersihCleaningService(cmd[0]);
                    break;
                default:
                    throw new IOException("Query not Found");
            }
        } else if (size == 0) {
            switch (query) {
                case "TOTAL_SEMBUH_MANUSIA":
                    totalSembuhManusia();
                    break;
                case "EXIT":
                    command = "PROGRAM BERHENTI!!!!";
                    pw.close();
                    break;
                default:
                    throw new IOException("Query not Found");
            }
        }
    }

    /**
     * Membuat objek add untuk menambahkan objek carrier ke listCarrier.
     * @param tipe sebagai tipe objek.
     * @param nama sebagai nama objek.
     */
    public void add(String tipe, String nama) throws IOException {
        Carrier carrier = world.getCarrier(nama);
        // jika objek tidak ada di listCarrier maka tambahkan ke listCarrier
        if (carrier == null) {
            Carrier createObject = world.createObject(tipe, nama);
            // jika tipe tidak ada maka lemparkan exception
            if (createObject == null) {
                throw new IOException("Tipe is not found");
            // jika tipe ada maka tambahkan ke listCarrier
            } else {
                createObject.ubahStatus("negatif");
                world.listCarrier.add(createObject);
                command = "Berhasil menambahkan " + createObject.toString();
            }
        // jika objek sudah ada di listCarrier maka lemparkan exception
        } else {
            throw new IOException("Name already exist in listCarrier");
        }
    }

    /**
     * Membuat objek interaksi untuk melakukan interaksi antara 2 objek.
     * @param object1 sebagai objek carrier 1.
     * @param object2 sebagai objek carrier 2.
     */
    public void interaksi(String object1, String object2) throws IOException {
        Carrier carrier1 = world.getCarrier(object1);
        Carrier carrier2 = world.getCarrier(object2);
        // jika objek 1 dan 2 tidak ada di listCarrier maka lemparkan exception
        if (carrier1 == null && carrier2 == null) {
            throw new IOException(object1 + " and " + object2 + " not exists in listCarrier");
        // jika objek 1 tidak ada di listCarrier maka lemparkan exception
        } else if (carrier1 == null) {
            throw new IOException(object1 + " not exist in listCarrier");
        // jika objek 2 tidak ada di listCarrier maka lemparkan exception
        } else if (carrier2 == null) {
            throw new IOException(object2 + " not exist in listCarrier");
        // jika keduanya ada maka akan berinteraksi
        } else {
            String tipeCarrier1 = carrier1.getStatusCovid();
            String tipeCarrier2 = carrier2.getStatusCovid();
            // jika objek 1 positif dan objek 2 negatif maka melakukan interaksi
            if (tipeCarrier1.equals("Positif") && tipeCarrier2.equals("Negatif")) {
                carrier1.interaksi(carrier2);
            // jika objek 2 positif dan objek 1 negatif maka melakukan interaksi
            } else if (tipeCarrier2.equals("Positif") && tipeCarrier1.equals("Negatif")) {
                carrier2.interaksi(carrier1);
            }
            command = "Berhasil melakukan interaksi antara "
                    + carrier1.toString() + " dengan " + carrier2.toString();
        }
    }

    /**
     * Membuat objek positifkan untuk mempositfkan status Covid objek.
     * @param object sebagai objek carrier.
     */
    public void positifkan(String object) throws IOException {
        Carrier carrier = world.getCarrier(object);
        // jika objek tidak ada di listCarrier maka lemparkan exception
        if (carrier == null) {
            throw new IOException(object + " not exist in listCarrier");
        // jika objek bukan manusia maka lemparkan exception
        } else if (!carrier.getTipe().equals("Manusia")) {
            throw new IOException(object + " is Benda");
        // jika objek manusia dan ada di listCarrier maka positifkan
        } else {
            carrier.ubahStatus("positif");
            carrier.getRantaiPenular().add(carrier);
            command = "Berhasil positifkan " + carrier.toString();
        }
    }

    /**
     * Membuat objek sembuhkan untuk menyembuhkan objek dari positif Covid.
     * @param object1 sebagai objek carrier 1.
     * @param object2 sebagai objek carrier 2.
     */
    public void sembuhkan(String object1, String object2) throws IOException {
        Carrier carrier1 = world.getCarrier(object1);
        Carrier carrier2 = world.getCarrier(object2);
        // jika objek 1 dan 2 tidak ada di listCarrier maka lemparkan exception
        if (carrier1 == null && carrier2 == null) {
            throw new IOException(object1 + " and " + object2 + " not exists in listCarrier");
        // jika objek 1 tidak ada di listCarrier maka lemparkan exception
        } else if (carrier1 == null) {
            throw new IOException(object1 + " is not exist in listCarrier");
        // jika objek 2 tidak ada di listCarrier maka lemparkan exception
        } else if (carrier2 == null) {
            throw new IOException(object2 + " is not exist in listCarrier");
        // jika objek 1 bukan petugas medis maka lemparkan exception
        } else if (!(carrier1 instanceof PetugasMedis)) {
            throw new IOException(object1 + " is not PETUGAS MEDIS");
        // jika objek 2 bukan manusia maka lemparkan exception
        } else if (!(carrier2 instanceof Manusia)) {
            throw new IOException(object2 + " is not Manusia");
        // jika objek 2 adalah manusia namun berstatus negatif covid
        // maka lemparkan exception
        } else if (!carrier2.getStatusCovid().equals("Positif")) {
            throw new IOException(object2 + " is status Negatif");
        // jika kriteria memenuhi maka objek 2 akan disembuhkan oleh objek 1
        } else {
            PetugasMedis petugasMedis = (PetugasMedis)carrier1;
            Manusia manusia = (Manusia)carrier2;
            petugasMedis.obati(manusia);
            carrier2.getRantaiPenular().clear();
            ArrayList<Carrier> x = new ArrayList<>();
            // melakukan iterasi dari listCarrier untuk mengurangi aktifKasusDisebabkan
            for (Carrier list: world.listCarrier) {
                // jika objek merupakan orang pertama positif maka tidak akan dikurangi
                if (list.getRantaiPenular().size() != 1) {
                    x = carrier2.sembuh(list, x);
                }
            }
            command = "Berhasil menyembuhkan " + carrier2.toString()
                    + " oleh " + carrier1.toString();
        }
    }

    /**
     * Membuat objek bersihkan untuk membersihkan objek
     * dari positif Covid dan persentase menular.
     * @param object1 sebagai objek carrier 1.
     * @param object2 sebagai objek carrier 2.
     */
    public void bersihkan(String object1, String object2) throws IOException {
        Carrier carrier1 = world.getCarrier(object1);
        Carrier carrier2 = world.getCarrier(object2);
        // jika objek 1 dan 2 tidak ada di listCarrier maka lemparkan exception
        if (carrier1 == null && carrier2 == null) {
            throw new IOException(object1 + " and " + object2 + " not exists in listCarrier");
        // jika objek 1 tidak ada di listCarrier maka lemparkan exception
        } else if (carrier1 == null) {
            throw new IOException(object1 + " is not exist in listCarrier");
        // jika objek 2 tidak ada di listCarrier maka lemparkan exception
        } else if (carrier2 == null) {
            throw new IOException(object2 + " is not exist in listCarrier");
        // jika objek 1 bukan cleaning service maka lemparkan exception
        } else if (!(carrier1 instanceof CleaningService)) {
            throw new IOException(object1 + " is not CLEANING SERVICE");
        // jika objek 2 bukan benda maka lemparkan exception
        } else if (!(carrier2 instanceof Benda)) {
            throw new IOException(object2 + " is not Benda");
        // jika kriteria memenuhi maka objek 2 akan dibersihkan oleh objek 1
        } else {
            CleaningService cleaningService = (CleaningService) carrier1;
            Benda benda = (Benda)carrier2;
            int persenBenda = benda.getPersentaseMenular();
            cleaningService.bersihkan(benda);
            carrier2.getRantaiPenular().clear();
            if (persenBenda >= 100) {
                ArrayList<Carrier> x = new ArrayList<>();
                // melakukan iterasi dari listCarrier untuk mengurangi aktifKasusDisebabkan
                for (Carrier list : world.listCarrier) {
                    // jika objek merupakan orang pertama positif maka tidak akan dikurangi
                    if (list.getRantaiPenular().size() != 1) {
                        x = carrier2.sembuh(list, x);
                    }
                }
            }
            command = "Berhasil menmbersihkan " + carrier2.toString()
                    + " oleh " + carrier1.toString();
        }
    }

    /**
     * Membuat objek rantai untuk mencetak riwayat rantai peularan dari objek.
     * @param object sebagai objek carrier.
     */
    public void rantai(String object) throws IOException {
        Carrier carrier = world.getCarrier(object);
        // jika objek tidak ada di listCarrier maka lemparkan exception
        if (carrier == null) {
            throw new IOException(object + " is not exist in listCarrier");
        // jika ada di list maka akan dicek lagi
        } else {
            String statusCarrier = carrier.getStatusCovid();
            int size = 1;
            try {
                // jika objek berstatus negatif maka akan melempar BelumTertularException.
                if (!statusCarrier.equals("Positif")) {
                    throw new BelumTertularException(carrier.toString() + " berstatus negatif");
                // jika memenuhi maka akan dicetak sesuai dengan soal.
                } else {
                    String x = "";
                    x += "Rantai penyebaran " + carrier.toString() + ": ";
                    for (Carrier list: carrier.getRantaiPenular()) {
                        if (size == carrier.getRantaiPenular().size()) {
                            x += list.toString();
                        } else {
                            x += list.toString() + " -> ";
                            size++;
                        }
                    }
                    pw.println(x);
                    pw.flush();
                    command = x;
                }
            // mencetak hasil lemparan dari objek berstatus negatif
            } catch (BelumTertularException e) {
                pw.println("assignments.assignment3.BelumTertularException: " + e.getMessage());
                pw.flush();
                command = e.toString();
            }
        }
    }

    /**
     * Membuat objek totalKasusDariObjek untuk mencetak
     * total kasus yang disebabkan oleh objek.
     * @param object sebagai objek carrier.
     */
    public void totalKasusDariObjek(String object) throws IOException {
        Carrier carrier = world.getCarrier(object);
        // jika objek tidak ada di listCarrier maka lemparkan exception
        if (carrier == null) {
            throw new IOException(object + " is not exist in listCarrier");
        // jika memenuhi maka akan dicetak sesuai dengan soal.
        } else {
            pw.println(carrier.toString() + " telah menyebarkan virus COVID ke "
                    + carrier.getTotalKasusDisebabkan() + " objek");
            command = carrier.toString() + " telah menyebarkan virus COVID ke "
                    + carrier.getTotalKasusDisebabkan() + " objek";
            pw.flush();
        }
    }

    /**
     * Membuat objek aktifKasusDariObjek untuk mencetak
     * aktif kasus yang disebabkan oleh objek.
     * @param object sebagai objek carrier.
     */
    public void aktifKasusdariObjek(String object) throws IOException {
        Carrier carrier = world.getCarrier(object);
        // jika objek tidak ada di listCarrier maka lemparkan exception
        if (carrier == null) {
            throw new IOException(object + " is not exist in listCarrier");
        // jika memenuhi maka akan dicetak sesuai dengan soal.
        } else {
            pw.println(carrier.toString()
                    + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak "
                    + carrier.getAktifKasusDisebabkan() + " objek");
            command = carrier.toString()
                    + " telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak "
                    + carrier.getAktifKasusDisebabkan() + " objek";
            pw.flush();
        }
    }

    /**
     * Membuat objek totalSembuhManusia untuk mencetak
     * total sembuh manusia dari Covid.
     */
    public void totalSembuhManusia() {
        pw.println("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: "
                + Manusia.getJumlahSembuh() + " kasus");
        command = "Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: "
                + Manusia.getJumlahSembuh() + " kasus";
    }

    /**
     * Membuat objek totalSembuhPetugasMedis untuk mencetak
     * total manusia yang sembuh oleh objek petugas medis.
     * @param object sebagai objek carrier.
     */
    public void totalSembuhPetugasMedis(String object) throws IOException {
        Carrier carrier = world.getCarrier(object);
        // jika objek  tidak ada di listCarrier maka lemparkan exception
        if (carrier == null) {
            throw new IOException(object + " is not exist in listCarrier");
        // jika objek  bukan petugas medis maka lemparkan exception
        } else if (!(carrier instanceof PetugasMedis)) {
            throw new IOException(object + " is not PETUGAS MEDIS");
        // jika memenuhi maka akan dicetak sesuai dengan soal.
        } else {
            PetugasMedis petugasMedis = (PetugasMedis)carrier;
            pw.println(carrier.toString() + " menyembuhkan "
                    + petugasMedis.getJumlahDisembuhkan() + " manusia");
            command = carrier.toString() + " menyembuhkan "
                    + petugasMedis.getJumlahDisembuhkan() + " manusia";
            pw.flush();
        }
    }

    /**
     * Membuat objek totalBersihCleaningService untuk mencetak
     * total benda yang sudah bersih oleh objek cleaning service.
     * @param object sebagai objek carrier.
     */
    public void totalBersihCleaningService(String object) throws IOException {
        Carrier carrier = world.getCarrier(object);
        // jika objek tidak ada di listCarrier maka lemparkan exception
        if (carrier == null) {
            throw new IOException(object + " is not exist in listCarrier");
        // jika objek  bukan cleaning service maka lemparkan exceptionelse
        } else if (!(carrier instanceof CleaningService)) {
            throw new IOException(object + " is not CLEANING SERVICE");
        // jika memenuhi maka akan mencetak sesuai soal.
        } else {
            CleaningService cleaningService = (CleaningService) carrier;
            pw.println(carrier.toString() + " membersihkan "
                    + cleaningService.getJumlahDibersihkan() + " benda");
            command = carrier.toString() + " membersihkan "
                    + cleaningService.getJumlahDibersihkan() + " benda";
            pw.flush();
        }
    }

    /**
     * Membuat objek exit untuk mngembalikan true.
     * berfungsi untuk menghentikan program di Simulator.
     * @return true.
     */
    public boolean exit() {
        return true;
    }

    /**
     * Membuat objek getCommand untuk mngembalikan command.
     * berfungsi untuk mencetak bahwa query telah berhasil di textArea pada SimulatorGUI.
     * @return command.
     */
    public String getCommand() {
        return command;
    }
}