package assignments.assignment3;

public class PekerjaJasa extends Manusia {
    /**
     * Membuat objek PekerjaJasa sebagai constructor.
     * @param nama sebagai nama PekerjaJasa.
     */
    public PekerjaJasa(String nama) {
        super(nama);
    }

    /**
     * Membuat objek toString untuk mengembalikan representasi String.
     * sebuah PekerjaJasa sesuai permintaan soal.
     * @return String.
     */
    @Override
    public String toString() {
        return "PEKERJA JASA " + this.getNama();
    }
}